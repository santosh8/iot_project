import boto3
import json
import os
from PIL import Image

def get_total_files_size():
    directory = os.path.join(os.getcwd(), 'cache')
    total_size = 0
    for filename in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, filename)):
            total_size += os.path.getsize(os.path.join(directory, filename))
    return total_size

def get_local_files():
    directory = os.path.join(os.getcwd(), 'cache')
    files = []
    for filename in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, filename)):
            files.append(filename)
    return files

def categorize_files(files):
    pdfs = []
    images = [] 
    videos = []
    misc = []
    for file in files:
        if file.endswith(".pdf"):
            pdfs.append(file)
        elif file.endswith(".jpg") or file.endswith(".png") or file.endswith("JPG") or file.endswith("jpeg"):
            images.append(file)
        elif file.endswith(".mp4"):
            videos.append(file)
        else:
            misc.append(file)
    return pdfs, images, videos, misc

def get_s3_files():
    with open("creds.json", "r") as f:
        creds = json.load(f)
    s3 = boto3.client('s3',aws_access_key_id=creds["access_key"], aws_secret_access_key=creds["secret"])
    s3_files = []
    s3_data = s3.list_objects(Bucket="hnas")
    if 'Contents' in s3_data:
        for key in s3_data['Contents']:
            s3_files.append(key['Key'])
    return s3_files

def get_file_size(filename):
    # check if file exists
    directory = os.path.join(os.getcwd(), 'cache')
    if os.path.isfile(os.path.join(directory, filename)):
        return os.path.getsize(os.path.join(directory, filename))
    else:
        with open("creds.json", "r") as f:
            creds = json.load(f)
        s3 = boto3.client('s3',aws_access_key_id=creds["access_key"], aws_secret_access_key=creds["secret"])
        file_data = s3.get_object(Bucket="hnas", Key=filename)
        return file_data['ContentLength']

def get_file_with_local_mode():
    with open("save_mode.json", "r") as f:
        save_mode_data = json.load(f)
    local_files = []
    for key in save_mode_data:
        if save_mode_data[key] == "local":
            local_files.append(key)
    return local_files

def get_file_with_s3_mode():
    with open("save_mode.json", "r") as f:
        save_mode_data = json.load(f)
    s3_files = []
    for key in save_mode_data:
        if save_mode_data[key] == "s3":
            s3_files.append(key)
    return s3_files

def get_file_with_auto_mode():
    with open("save_mode.json", "r") as f:
        save_mode_data = json.load(f)
    auto_files = []
    for key in save_mode_data:
        if save_mode_data[key] == "auto":
            auto_files.append(key)
    return auto_files


def generate_thumb(filename):
    """generate thumbnail for image and save in /thubmnails folder"""
    directory = os.path.join(os.getcwd(), 'cache')
    im = Image.open(os.path.join(directory, filename))
    im.thumbnail((128, 128))
    im.save(os.path.join('thumb', filename))