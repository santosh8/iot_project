# H-NAS: Hybrid NAS with Cloud Storage and Local Caching

Developed by:

Name: Santosh Chejarla

NetID: santosh8

## Setup:

*Requirements:*
- Python3
- AWS S3
- Raspberry Pi
- Pendrive/SD card/Hard disk
- Extra python libraries:
    - json
    - boto3
    - Flask


1. ```git clone https://gitlab.engr.illinois.edu/santosh8/iot_project/-/tree/master```
2. ```sudo chmod -R 777 iot_project```
3. ```cd iot_project```
4. ```sudo python3 app.py```
