from flask import Flask, render_template, request, redirect, url_for, flash, session, send_file, abort, send_from_directory
import os
import boto3
import json
from constants import *
from utils import *
from lfu_checker import LFUChecker
import logging
logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
import datetime

app = Flask(__name__)
app.secret_key = 'your_secret_key_here'
app.config["CACHE_TYPE"] = "null"
@app.route('/download/<path:filename>', methods=['GET'])
def download_file(filename):
    try:
        logging.info("File: " + filename + " downloaded by " + request.remote_addr + " at " + str(datetime.datetime.now()) + " with user agent " + request.user_agent.string)
    except Exception as e:
        logging.exception("Exception occurred: {}".format)
    if not check_login():
        return redirect(url_for('login'))
    try:
        local_files = get_local_files()
        s3_files = get_s3_files()
        with open("access_data.json", "r") as f:
            access_data = json.load(f)
            access_data[filename] += 1
        with open("access_data.json", "w") as f:
            json.dump(access_data, f)
        if filename in local_files:
            return send_file('./cache/' + filename, as_attachment=True)
        elif filename in s3_files:
            return redirect("https://s3.console.aws.amazon.com/s3/object/hnas?region=us-east-2&prefix=" + filename)
    except FileNotFoundError:
        abort(404)

@app.route('/thumb/<path:filename>')
def serve_thumb(filename):
    return send_from_directory('./thumb', filename)

def check_login():
    try:
        logging.info("User: " + session['username'] + " logged in at " + str(datetime.datetime.now()) + " with user agent " + request.user_agent.string)
    except Exception as e:
        logging.exception("Exception occurred: {}".format)
    if 'username' not in session:
        return redirect(url_for('login'))
    else:            
        files = set(get_local_files() + get_s3_files())
        pdfs, images, videos, misc = categorize_files(files)
        mpdfs = True if len(pdfs) > 5 else False
        mimages = True if len(images) > 5 else False
        mvideos = True if len(videos) > 5 else False
        mmisc = True if len(misc) > 5 else False
        return render_template('index.html',pdffiles=pdfs[:5], imgfiles=images[:5], videofiles=videos[:5], misc=misc[:5], mpdfs=mpdfs, mimages=mimages, mvideos=mvideos, mmisc=mmisc)
    

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if (username == 'admin') and (password == 'admin'):
            session['username'] = username
            return redirect(url_for('index'))
        else:
            flash('Invalid username or password')
            return redirect(url_for('login'))
        
    return render_template('login.html')

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if not check_login():
        return redirect(url_for('login'))

    if request.method == 'POST':
        file = request.files['file']
        filename = file.filename
        with open("access_data.json", "r+") as f:
            access_data = json.load(f)
            access_data[filename] = 0
        with open("access_data.json", "w") as f:
            json.dump(access_data, f)
        location = request.form.get('location')
        with open("save_mode.json", "r+") as f:
            save_mode_data = json.load(f)
            save_mode_data[filename] = location
        with open("save_mode.json", "w") as f:
            json.dump(save_mode_data, f)
        if location == "local":
            directory = os.path.join(os.getcwd(), 'cache')
            tot_size = get_total_files_size()
            file_size = file.seek(0, os.SEEK_END)
            file.seek(0)
            if tot_size + file_size > CACHE_SIZE:
                files_with_auto_mode = get_file_with_auto_mode()
                lfu_obj = LFUChecker(CACHE_SIZE)
                lfu_files = lfu_obj.get_lfu_files()
                for file in lfu_files:
                    if file in files_with_auto_mode:
                        os.remove(os.path.join(os.getcwd(), 'cache', file))
                    tot_size = get_total_files_size()
                    if tot_size + file_size <= CACHE_SIZE:
                        break
            tot_size = get_total_files_size()
            if tot_size + file_size > CACHE_SIZE:
                return redirect(url_for('upload_fail'))
            file.save(os.path.join(directory, filename))
        elif location == "s3":
            with open("creds.json", "r") as f:
                creds = json.load(f)
            s3 = boto3.client('s3',aws_access_key_id=creds["access_key"], aws_secret_access_key=creds["secret"])
            s3.upload_fileobj(file, "hnas", filename)
        else:
            file_path = os.path.join(os.getcwd(), 'cache', filename)
            tot_size = get_total_files_size()
            file_size = file.seek(0, os.SEEK_END)
            file.seek(0)
            if tot_size + file_size > CACHE_SIZE:
                files_with_auto_mode = get_file_with_auto_mode()
                lfu_obj = LFUChecker(CACHE_SIZE)
                lfu_files = lfu_obj.get_lfu()
                for _file in lfu_files:
                    if lfu_files[_file] > 0: continue
                    if _file == filename: continue
                    if _file in files_with_auto_mode:
                        os.remove(os.path.join(os.getcwd(), 'cache', _file))
                    tot_size = get_total_files_size()
                    if tot_size + file_size <= CACHE_SIZE:
                        break
            tot_size = get_total_files_size()
            if tot_size + file_size <= CACHE_SIZE:
                file.save(file_path)
                file.seek(0)
            with open("creds.json", "r") as f:
                creds = json.load(f)
            s3 = boto3.client('s3',aws_access_key_id=creds["access_key"], aws_secret_access_key=creds["secret"])
            s3.upload_fileobj(file, "hnas", filename)
        if "jpeg" in filename or "jpg" in filename or "png" in filename or "JPG" in filename:
            generate_thumb(filename)   
        return redirect(url_for('index'))

    return render_template('upload.html')

@app.route('/images')
def images():
    if not check_login():
        return redirect(url_for('login'))
    files = set(get_local_files() + get_s3_files())
    pdfs, images, videos, misc = categorize_files(files)
    return render_template('more_files.html', files=images, content='Image', fa_content='icon-camera')

@app.route('/videos')
def videos():
    if not check_login():
        return redirect(url_for('login'))
    files = set(get_local_files() + get_s3_files())
    pdfs, images, videos, misc = categorize_files(files)
    return render_template('more_files.html', files=videos, content='Video', fa_content='icon-facetime-video')

@app.route('/pdfs')
def pdfs():
    if not check_login():
        return redirect(url_for('login'))
    files = set(get_local_files() + get_s3_files())
    pdfs, images, videos, misc = categorize_files(files)
    return render_template('more_files.html', files=pdfs, content='Pdf', fa_content='icon-file-text')

@app.route('/misc')
def misc():
    if not check_login():
        return redirect(url_for('login'))
    files = set(get_local_files() + get_s3_files())
    pdfs, images, videos, misc = categorize_files(files)
    return render_template('more_files.html', files=misc, content='Misc.', fa_content='icon-sun')


@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('login'))

@app.route('/upload_fail')
def upload_fail():
    return render_template('upload_fail.html')


@app.route('/delete_page')
def delete_page():
    if not check_login():
        return redirect(url_for('login'))
    files = set(get_local_files() + get_s3_files())
    return render_template('delete.html', files=files)

@app.route('/delete', methods=['POST'])
def delete_file():
    if not check_login():
        return redirect(url_for('login'))

    filename = request.form.get('delete-file')
    try:
        logging.info("Deletion User: %s, File: %s, Time: %s", session['username'], filename, datetime.datetime.now())
    except Exception as e:
        logging.exception("Exception occured:{}".format(e))

    if filename:
        local_file_path = os.path.join(os.getcwd(), 'cache', filename)
        s3_file_path = filename
        if os.path.exists(local_file_path):
            os.remove(local_file_path)
        else:
            with open("creds.json", "r") as f:
                creds = json.load(f)
            s3 = boto3.client('s3', aws_access_key_id=creds["access_key"], aws_secret_access_key=creds["secret"])
            s3.delete_object(Bucket="hnas", Key=s3_file_path)
        with open("access_data.json", "r+") as f:
            access_data = json.load(f)
            access_data.pop(filename, None)
            f.seek(0)
            json.dump(access_data, f)
            f.truncate()
        with open("save_mode.json", "r+") as f:
            save_mode_data = json.load(f)
            save_mode_data.pop(filename, None)
            f.seek(0)
            json.dump(save_mode_data, f)
            f.truncate()

    return redirect(url_for('index'))


# index page
@app.route('/')
def index():
    return check_login()

if __name__ == '__main__':
    app.run(port=80, host="0.0.0.0", debug=True)
