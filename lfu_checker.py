from constants import *
from utils import *
import json
import boto3

class LFUChecker:
    def __init__(self, cache_size):
        self.cache = cache_size

    def get_files(self):
        local_files = set(get_local_files())
        s3_files = set(get_s3_files())
        return local_files, s3_files
    
    def get_access_data(self):
        with open("access_data.json", "r") as f:
            access_data = json.load(f)
        return access_data
    
    def get_lfu(self, sort_val = 1):
        local_files, s3_files = self.get_files()
        access_data = self.get_access_data()
        lfu = {}
        for file in local_files:
            if file in access_data:
                lfu[file] = access_data[file]
            else:
                lfu[file] = 0
        for file in s3_files:
            if file in access_data:
                lfu[file] = access_data[file]
            else:
                lfu[file] = 0
        return dict(sorted(lfu.items(), key=lambda item: sort_val * item[1]))
    
    def transer_files(self):
        lfu_files = self.get_lfu(sort_val=-1)
        with open("save_mode.json", "r") as f:
            save_mode_data = json.load(f)
        curr_size = 0
        local_files = get_local_files()
        files_with_local_mode = get_file_with_local_mode()
        fill = False
        for file in files_with_local_mode:
            curr_size += get_file_size(file)
        for file in lfu_files:
            if fill and save_mode_data[file]=="auto":
                if file in local_files:
                    os.remove(os.path.join(os.getcwd(), 'cache', file))
                continue
            if save_mode_data[file]=="local":
                continue
            elif save_mode_data[file]=="s3":
                continue
            else:
                if curr_size + get_file_size(file) > self.cache:
                    if file in local_files:
                        os.remove(os.path.join(os.getcwd(), 'cache', file))
                    fill = True
                else:
                    curr_size += get_file_size(file)
                    if file in local_files:
                        continue
                    else:
                        # download file from s3
                        with open("creds.json", "r") as f:
                            creds = json.load(f)
                        s3 = boto3.client('s3',aws_access_key_id=creds["access_key"], aws_secret_access_key=creds["secret"])
                        s3.download_file("hnas", file, os.path.join(os.getcwd(), 'cache', file))
    
if __name__=="__main__":
    lfu_checker = LFUChecker(CACHE_SIZE)
    lfu_checker.transer_files()


        

    